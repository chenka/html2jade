// modules =================================================
var express        = require('express');
var app            = express();
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var _ = require('lodash');
var server = require('http').Server(app);
var io = require('socket.io')(server);
var html2Jade = require('html2jade');
var jade = require('jade');
// configuration ===========================================

// config files

var port = process.env.PORT || 8080; // set our port

// get all data/stuff of the body (POST) parameters
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded

app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users

// routes ==================================================
require('./app/routes')(app); // pass our application into our routes

app.get('*', function (req,res) {
  res.send(404, 'File Not Found')
})
// start app ===============================================
console.log('Starting app on port ' + port);
server.listen(port);
io.on('connection', function (socket) {
  socket.on('edit html', function (html, options) {
    if (html) {
      options = {
        bodyless: options.bodyless,
        nspaces: options.nspaces,
        donotencode: true,
        tabs: options.tabs
      }

      html2Jade.convertHtml(html, options, function (err, jade) {
        jade = jade.replace(/\|\s+$/gm, '');
        jade = jade.replace(/^(?:[\t ]*(?:\r?\n|\r))+/gm, '');
        socket.emit('html2jade', jade);
      });
    }
  });


  socket.on('edit jade', function (jadeText, options) {
    if (jadeText) {
      try {
        html = jade.render(jadeText, { pretty: true });
        html = html.replace(/^(?:[\t ]*(?:\r?\n|\r))+/gm, '');
        html = html.replace(/\s+$/gm, '');
        socket.emit('jade2html', {html: html});
      }catch(e){
        socket.emit('jade2html', {error: e.message});
      }
    }
  });

});
exports = module.exports = app; 						// expose app
